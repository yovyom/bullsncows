import time
import random
from itertools import permutations


class BullsNCows:
    def __init__(self):
        # Number of places
        self.p = 4
        self.symbols = "0123456789"

    def get_all_secrets(self):
        return ["".join(i) for i in list(permutations(self.symbols, self.p))]

    def is_solution_possible(self, secret, guess, bulls, cows):
        x = self.get_bulls_n_cows(secret, guess)
        return x[0] == bulls and x[1] == cows

    def get_bulls_n_cows(self, secret, guess):
        cows = len(set(secret).intersection(set(guess)))
        bulls = 0
        for _, pair in enumerate(zip(secret, guess)):
            if pair[0] == pair[1]:
                bulls += 1
        return (bulls, cows - bulls)

    def get_next_guess(self, secrets):
        return secrets[random.randint(0, len(secrets) - 1)]

    def play(self, verbose=False):
        secrets = self.get_all_secrets()

        turns = 0

        while True:
            guess = secrets[random.randint(0, len(secrets) - 1)]
            turns += 1
            print(f"Guess #{turns}, Guessed number: {guess}, Feedback: ", end="")
            # feedback has to be two comma separated integers in the range 0 and 4
            feedback = input().split(",")
            bulls, cows = int(feedback[0]), int(feedback[1])

            if bulls == 4:
                break

            secrets = [
                s for s in secrets if self.is_solution_possible(s, guess, bulls, cows)
            ]
            if verbose:
                print(f"Remaining possiblities: {len(secrets)}")

    def play_automated(self, games_per_secret=1, verbose=True):
        all_secrets = self.get_all_secrets()
        print(f"Games to be played {len(all_secrets)*games_per_secret} ...", flush=True)
        t1 = time.time()
        games = 0
        attempts = []
        for _ in range(0, games_per_secret):
            for secret_number in all_secrets:
                history = {}
                games += 1
                secrets = self.get_all_secrets()

                turns = 0
                while True:
                    guess = self.get_next_guess(secrets)
                    turns += 1
                    bulls, cows = self.get_bulls_n_cows(secret_number, guess)

                    if bulls == 4:
                        attempts.append(turns)
                        break

                    secrets = [
                        s
                        for s in secrets
                        if self.is_solution_possible(s, guess, bulls, cows)
                    ]

                if verbose:
                    if games % 100 == 0:
                        print(f"Games done {games:>4}.", flush=True)
        t2 = time.time()
        print()
        print(f"Total run time: {t2-t1:>0.5} seconds.")
        print(f"Turns | Frequency")
        for i in range(1, 11):
            print(f"{i:>5} | {attempts.count(i):>9}")
        print(f"Average game length: {sum(attempts)/len(attempts):>0.5}")


if __name__ == "__main__":
    bc = BullsNCows()
    bc.play(True)

