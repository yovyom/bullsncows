import time
from itertools import permutations
from enum import Enum


class NextGuessPolicy(Enum):
    FROM_REMAINING = 1
    FROM_ALL = 2
    FROM_ALL_PREF_REMAINING = 3


class BullsNCows:
    def __init__(self, next_guess_policy):
        # Number of places
        self.p = 4
        self.symbols = "0123456789"
        self.answer = (4, 0)
        self.all_secrets = self.get_all_secrets()
        self.next_guess_policy = next_guess_policy

    def get_all_secrets(self):
        return ["".join(i) for i in list(permutations(self.symbols, self.p))]

    def is_solution_possible(self, secret, guess, bulls, cows):
        x = self.get_bulls_n_cows(secret, guess)
        return x[0] == bulls and x[1] == cows

    def get_bulls_n_cows(self, secret, guess):
        cows = len(set(secret).intersection(set(guess)))
        bulls = 0
        for _, pair in enumerate(zip(secret, guess)):
            if pair[0] == pair[1]:
                bulls += 1
        return (bulls, cows - bulls)

    def get_fan_out_map(self, guess, secrets):
        fm = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x in fm:
                fm[x] = fm[x] + 1
            else:
                fm[x] = 1
        return fm

    def get_next_guess(self, secrets):
        if len(secrets) == 1 or len(secrets) == 2:
            return secrets[0]

        if self.next_guess_policy == NextGuessPolicy.FROM_REMAINING:
            guesses = secrets
        else:
            guesses = self.all_secrets

        max_fan_out = -1
        guess = None
        max_fan_out_remaining = -1
        guess_from_remaining = None
        for g in guesses:
            fm = self.get_fan_out_map(g, secrets)
            if len(fm) > max_fan_out:
                max_fan_out = len(fm)
                guess = g
            if (
                self.next_guess_policy == NextGuessPolicy.FROM_ALL_PREF_REMAINING
                and g in secrets
                and len(fm) > max_fan_out_remaining
            ):
                max_fan_out_remaining = len(fm)
                guess_from_remaining = g

        if max_fan_out_remaining == max_fan_out:
            return guess_from_remaining
        else:
            return guess

    def solve(self):
        turns_map = {}

        t1 = time.time()
        self.__solve("0123", self.all_secrets, 1, turns_map)
        t2 = time.time()

        print(f"Total run time: {t2-t1:>0.5} seconds.")
        print(turns_map)

        s = 0
        ws = 0
        for k, v in turns_map.items():
            s = s + v
            ws = ws + k * v
        assert len(self.all_secrets) == s
        print(
            f"Total Secrets: {s}, Total Turns: {ws}, Average Game Length: {ws/s:>0.5}"
        )

    def __solve(self, guess, secrets, level, t_m):
        if len(secrets) == 0:
            return

        m = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x not in m:
                m[x] = 1
            else:
                m[x] += 1
        if self.answer in m:
            if level not in t_m:
                t_m[level] = 1
            else:
                t_m[level] += 1
            del m[self.answer]

        F = [
            (0, 0),
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (1, 0),
            (1, 1),
            (1, 2),
            (1, 3),
            (2, 0),
            (2, 1),
            (2, 2),
            (3, 0),
            (4, 0),
        ]
        for f in F:
            if f in m:
                remaining = [
                    s
                    for s in secrets
                    if self.is_solution_possible(s, guess, f[0], f[1])
                ]
                next_guess = self.get_next_guess(remaining)
                self.__solve(next_guess, remaining, level + 1, t_m)

    def play(self, verbose=False):
        secrets = self.all_secrets

        guess = "0123"
        turns = 1

        while True:
            print(f"Guess #{turns}, Guessed number: {guess}, Feedback: ", end="")
            # feedback has to be two comma separated integers in the range 0 and 4
            feedback = input().split(",")
            bulls, cows = int(feedback[0]), int(feedback[1])

            if bulls == 4:
                break

            secrets = [
                s for s in secrets if self.is_solution_possible(s, guess, bulls, cows)
            ]
            if verbose:
                print(f"Remaining possiblities: {len(secrets)}")

            guess = self.get_next_guess(secrets)
            turns += 1

    def play_automated(self, games_per_secret=1, verbose=True):
        all_secrets = self.all_secrets
        print(f"Games to be played {len(all_secrets)*games_per_secret} ...", flush=True)
        t1 = time.time()
        games = 0
        attempts = []
        for _ in range(0, games_per_secret):
            for secret_number in all_secrets:
                history = {}
                games += 1
                secrets = self.all_secrets

                guess = "0123"
                turns = 1
                while True:
                    bulls, cows = self.get_bulls_n_cows(secret_number, guess)

                    if bulls == 4:
                        attempts.append(turns)
                        break

                    secrets = [
                        s
                        for s in secrets
                        if self.is_solution_possible(s, guess, bulls, cows)
                    ]
                    guess = self.get_next_guess(secrets)
                    turns += 1

                if verbose:
                    if games % 100 == 0:
                        print(f"Games done {games:>4}.", flush=True)
        t2 = time.time()
        print()
        print(f"Total run time: {t2-t1:>0.4} seconds.")
        print(f"Turns | Frequency")
        for i in range(1, 11):
            print(f"{i:>5} | {attempts.count(i):>9}")
        print(f"Average game length: {sum(attempts)/len(attempts):>0.4}")


if __name__ == "__main__":
    bc = BullsNCows(NextGuessPolicy.FROM_ALL_PREF_REMAINING)
    bc.solve()
    # Play game in interactive mode
    # bc.play(True)
    # Play game in automated mode
    # bc.play_automated()
