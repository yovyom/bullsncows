import random
from itertools import permutations


class Game:
    def __init__(self, random_guess=False):
        self.random_guess = random_guess

    def get_all_possible_secrets(self):
        return ["".join(i) for i in list(permutations("0123456789", 4))]

    def get_possible_feedbacks(self, secrets, guess):
        if len(secrets) == 1:
            assert secrets[0] == guess
            return [0]
        if secrets[0] == guess:
            return [0, 1]
        if secrets[len(secrets) - 1] == guess:
            return [0, -1]
        return [0, -1, 1]

    def next_guess(self, secrets):
        if self.random_guess:
            return secrets[random.randint(0, len(secrets) - 1)]
        else:
            return secrets[int(len(secrets) / 2)]

    def __solve(self, secrets, guess, turns, t_m):
        feedbacks = self.get_possible_feedbacks(secrets, guess)
        for f in feedbacks:
            if f == 0:
                if turns in t_m:
                    t_m[turns] += 1
                else:
                    t_m[turns] = 1
            elif f == -1:
                remaining = [s for s in secrets if s < guess]
                self.__solve(remaining, self.next_guess(remaining), turns + 1, t_m)
            else:
                remaining = [s for s in secrets if s > guess]
                self.__solve(remaining, self.next_guess(remaining), turns + 1, t_m)

    def solve(self):
        secrets = self.get_all_possible_secrets()
        guess = self.next_guess(secrets)

        turns_map = {}
        self.__solve(secrets, guess, 1, turns_map)
        print(turns_map)

        s = 0
        ws = 0
        for k, v in turns_map.items():
            s = s + v
            ws = ws + k * v

        assert len(secrets) == s
        print(
            f"Total Secrets: {s}, Total Turns: {ws}, Average Game Length: {ws/s:>0.4}"
        )


if __name__ == "__main__":
    g = Game()
    g.solve()
