def get_bulls_n_cows(secret, guess):
    cows = len(set(secret).intersection(set(guess)))
    bulls = 0
    for _, pair in enumerate(zip(secret, guess)):
        if pair[0] == pair[1]:
            bulls += 1
    return (bulls, cows - bulls)


def get_fan_out_map_detailed(guess, secrets):
    fm = {}
    for s in secrets:
        x = get_bulls_n_cows(s, guess)
        if x in fm:
            fm[x].append(s)
        else:
            fm[x] = [s]
    return fm


def print_fanout_map(m, spaces=0):
    for k, v in m.items():
        for i in range(spaces):
            print(" ", end="")
        print(k, v)
    print()


def get_best_one_look_ahead_guess(secrets):
    total_max_fan_out_aggregate = -1
    final_guess = None
    for g in ["2067", "7062"]:
        fm = get_fan_out_map_detailed(g, secrets)
        print(f"Guess: {g}, fan-out: {len(fm)}")
        print_fanout_map(fm)
        min_diff_guess = 0
        # Overall max fan-out, sum over all subsets
        total_max_fan_out = 0
        for k, v in fm.items():
            if len(v) == 1:
                total_max_fan_out += 1
            elif len(v) == 2:
                total_max_fan_out += 2
            else:
                # Max fan-out for a particular subset
                max_fan_out = -1
                best_subfm = None
                best_subguess = None
                for g1 in secrets:
                    fm1 = get_fan_out_map_detailed(g1, v)
                    if len(fm1) > max_fan_out:
                        max_fan_out = len(fm1)
                        best_subfm = fm1
                        best_subguess = g1
                print(
                    f"    feedback: {k} length-subset: {len(v)}, next-guess: {best_subguess}, max-fan-out: {max_fan_out}"
                )
                total_max_fan_out += max_fan_out
                print_fanout_map(best_subfm, spaces=8)
        print(f"    Guess: {g}, total_max_fan_out: {total_max_fan_out}")
        if total_max_fan_out + len(fm) > total_max_fan_out_aggregate:
            total_max_fan_out_aggregate = total_max_fan_out + len(fm)
            final_guess = g
    print(f"total_max_fan_out_aggregate: {total_max_fan_out_aggregate}")
    return final_guess


if __name__ == "__main__":
    # 68 secrets
    remaining_secrets = [
        "2067",
        "2069",
        "2075",
        "2095",
        "2507",
        "2509",
        "2570",
        "2590",
        "2607",
        "2609",
        "2670",
        "2690",
        "2705",
        "2719",
        "2760",
        "2791",
        "2905",
        "2917",
        "2960",
        "2971",
        "4017",
        "4019",
        "4071",
        "4091",
        "4701",
        "4710",
        "4901",
        "4910",
        "5072",
        "5092",
        "5702",
        "5902",
        "6072",
        "6092",
        "6702",
        "6902",
        "7014",
        "7039",
        "7062",
        "7081",
        "7401",
        "7410",
        "7502",
        "7602",
        "7801",
        "7810",
        "7912",
        "7930",
        "8017",
        "8019",
        "8071",
        "8091",
        "8701",
        "8710",
        "8901",
        "8910",
        "9014",
        "9037",
        "9062",
        "9081",
        "9401",
        "9410",
        "9502",
        "9602",
        "9712",
        "9730",
        "9801",
        "9810",
    ]
    best_guess = get_best_one_look_ahead_guess(remaining_secrets)
    print(f"Best One_look_ahead Guess: {best_guess}")

