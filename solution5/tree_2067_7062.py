import time
import random
import json
from itertools import permutations
from enum import Enum


class BullsNCows:
    def __init__(self):
        # Number of places
        self.p = 4
        self.symbols = "0123456789"
        self.answer = (4, 0)
        self.all_secrets = self.get_all_secrets()

    def get_all_secrets(self):
        return ["".join(i) for i in list(permutations(self.symbols, self.p))]

    def is_solution_possible(self, secret, guess, bulls, cows):
        x = self.get_bulls_n_cows(secret, guess)
        return x[0] == bulls and x[1] == cows

    def get_bulls_n_cows(self, secret, guess):
        cows = len(set(secret).intersection(set(guess)))
        bulls = 0
        for _, pair in enumerate(zip(secret, guess)):
            if pair[0] == pair[1]:
                bulls += 1
        return (bulls, cows - bulls)

    def get_fan_out_map(self, guess, secrets):
        fm = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x in fm:
                fm[x] = fm[x] + 1
            else:
                fm[x] = 1
        return fm

    def get_next_guess(self, secrets, feedbacks):
        if len(secrets) == 1 or len(secrets) == 2:
            return secrets[0]

        guesses = self.all_secrets

        max_fan_out = -1
        guess = None
        max_fan_out_remaining = -1
        guess_from_remaining = None
        for g in guesses:
            fm = self.get_fan_out_map(g, secrets)
            if len(fm) > max_fan_out:
                max_fan_out = len(fm)
                guess = g
            if g in secrets and len(fm) > max_fan_out_remaining:
                max_fan_out_remaining = len(fm)
                guess_from_remaining = g

        if max_fan_out_remaining == max_fan_out:
            return guess_from_remaining
        else:
            return guess

    def solve(self, secrets, guess):
        t1 = time.time()
        turns_map = {}
        print(f"Guess: {guess}")
        self.__solve(guess, secrets, 1, turns_map, [])
        t2 = time.time()

        print(f"Total run time: {t2-t1:>0.5} seconds.")
        print(turns_map)

        s = 0
        ws = 0
        max_key = 0
        for k, v in turns_map.items():
            s = s + v
            ws = ws + k * v
            if k > max_key:
                max_key = k
        print(
            f"Total Secrets: {s:>4}, Total Turns: {ws}, Average Game Length: {ws/s:>0.5}, Max turns: {max_key}"
        )
        print()

    def __solve(self, guess, secrets, level, t_m, feedbacks):
        if len(secrets) == 0:
            return

        m = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x not in m:
                m[x] = 1
            else:
                m[x] += 1
        if self.answer in m:
            if level not in t_m:
                t_m[level] = 1
            else:
                t_m[level] += 1
            del m[self.answer]

        F = [
            (0, 0),
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (1, 0),
            (1, 1),
            (1, 2),
            (1, 3),
            (2, 0),
            (2, 1),
            (2, 2),
            (3, 0),
            (4, 0),
        ]
        for f in F:
            if f in m:
                remaining = [
                    s
                    for s in secrets
                    if self.is_solution_possible(s, guess, f[0], f[1])
                ]
                n_feedbacks = feedbacks.copy()
                n_feedbacks.append(f)
                next_guess = self.get_next_guess(remaining, n_feedbacks)
                self.__solve(next_guess, remaining, level + 1, t_m, n_feedbacks)


if __name__ == "__main__":
    secrets = [
        "2067",
        "2069",
        "2075",
        "2095",
        "2507",
        "2509",
        "2570",
        "2590",
        "2607",
        "2609",
        "2670",
        "2690",
        "2705",
        "2719",
        "2760",
        "2791",
        "2905",
        "2917",
        "2960",
        "2971",
        "4017",
        "4019",
        "4071",
        "4091",
        "4701",
        "4710",
        "4901",
        "4910",
        "5072",
        "5092",
        "5702",
        "5902",
        "6072",
        "6092",
        "6702",
        "6902",
        "7014",
        "7039",
        "7062",
        "7081",
        "7401",
        "7410",
        "7502",
        "7602",
        "7801",
        "7810",
        "7912",
        "7930",
        "8017",
        "8019",
        "8071",
        "8091",
        "8701",
        "8710",
        "8901",
        "8910",
        "9014",
        "9037",
        "9062",
        "9081",
        "9401",
        "9410",
        "9502",
        "9602",
        "9712",
        "9730",
        "9801",
        "9810",
    ]
    bc = BullsNCows()
    bc.solve(secrets, "2067")
    bc.solve(secrets, "7062")

