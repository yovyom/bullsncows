import time
import json
import math
from itertools import permutations


class BullsNCows:
    def __init__(self):
        # Number of places
        self.p = 4
        self.symbols = "0123456789"
        self.answer = (4, 0)
        self.all_secrets = self.get_all_secrets()

    def get_all_secrets(self):
        return ["".join(i) for i in list(permutations(self.symbols, self.p))]

    def is_solution_possible(self, secret, guess, bulls, cows):
        x = self.get_bulls_n_cows(secret, guess)
        return x[0] == bulls and x[1] == cows

    def get_bulls_n_cows(self, secret, guess):
        cows = len(set(secret).intersection(set(guess)))
        bulls = 0
        for _, pair in enumerate(zip(secret, guess)):
            if pair[0] == pair[1]:
                bulls += 1
        return (bulls, cows - bulls)

    def get_fan_out_map(self, guess, secrets):
        fm = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x in fm:
                fm[x] = fm[x] + 1
            else:
                fm[x] = 1
        return fm

    def get_min_cost_guess(self, secrets):
        guesses = self.all_secrets
        cost = 999999
        min_cost_guess = None
        for g in guesses:
            fm = self.get_fan_out_map(g, secrets)
            c = self.get_guess_cost(fm)
            if c < cost:
                cost = c
                min_cost_guess = g
        return min_cost_guess

    def get_guess_cost(self, fm):
        c = 0
        for k, v in fm.items():
            if k == (4, 0):
                c = c - 2 * math.log(2)
            else:
                c = c + v * math.log(v)
        return c

    def get_next_guess(self, secrets, feedbacks):
        if len(secrets) == 1 or len(secrets) == 2:
            return secrets[0]

        return self.get_min_cost_guess(secrets)

    def solve(self):
        turns_map = {}

        t1 = time.time()
        self.__solve("0123", self.all_secrets, 1, turns_map, [])
        t2 = time.time()

        print(f"Total run time: {t2-t1:>0.5} seconds.")
        print(turns_map)

        s = 0
        ws = 0
        for k, v in turns_map.items():
            s = s + v
            ws = ws + k * v
        assert len(self.all_secrets) == s
        print(
            f"Total Secrets: {s}, Total Turns: {ws}, Average Game Length: {ws/s:>0.5}"
        )

    def __solve(self, guess, secrets, level, t_m, feedbacks):
        if len(secrets) == 0:
            return

        m = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x not in m:
                m[x] = 1
            else:
                m[x] += 1
        if self.answer in m:
            if level not in t_m:
                t_m[level] = 1
            else:
                t_m[level] += 1
            del m[self.answer]

        F = [
            (0, 0),
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (1, 0),
            (1, 1),
            (1, 2),
            (1, 3),
            (2, 0),
            (2, 1),
            (2, 2),
            (3, 0),
            (4, 0),
        ]
        for f in F:
            if f in m:
                remaining = [
                    s
                    for s in secrets
                    if self.is_solution_possible(s, guess, f[0], f[1])
                ]
                n_feedbacks = feedbacks.copy()
                n_feedbacks.append(f)
                next_guess = self.get_next_guess(remaining, n_feedbacks)
                self.__solve(next_guess, remaining, level + 1, t_m, n_feedbacks)


if __name__ == "__main__":
    bc = BullsNCows()
    bc.solve()
