import time
import json
import math
from itertools import permutations


class BullsNCows:
    def __init__(self):
        # Number of places
        self.p = 4
        self.symbols = "0123456789"
        self.answer = (4, 0)
        self.all_secrets = self.get_all_secrets()
        self.guess_map = {}

    def get_all_secrets(self):
        return ["".join(i) for i in list(permutations(self.symbols, self.p))]

    def is_solution_possible(self, secret, guess, bulls, cows):
        x = self.get_bulls_n_cows(secret, guess)
        return x[0] == bulls and x[1] == cows

    def get_bulls_n_cows(self, secret, guess):
        cows = len(set(secret).intersection(set(guess)))
        bulls = 0
        for _, pair in enumerate(zip(secret, guess)):
            if pair[0] == pair[1]:
                bulls += 1
        return (bulls, cows - bulls)

    def get_fan_out_map(self, guess, secrets):
        fm = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x in fm:
                fm[x] = fm[x] + 1
            else:
                fm[x] = 1
        return fm

    def get_fan_out_map_detailed(self, guess, secrets):
        lm = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x in lm:
                lm[x].append(s)
            else:
                lm[x] = [s]
        return lm

    def get_min_cost_guess(self, secrets):
        guesses = self.all_secrets
        cost = 999999
        min_cost_guess = None
        for g in guesses:
            fm = self.get_fan_out_map(g, secrets)
            c = self.get_guess_cost(fm)
            if c < cost:
                cost = c
                min_cost_guess = g
        return min_cost_guess

    def get_guess_cost(self, fm):
        c = 0
        for k, v in fm.items():
            if k == (4, 0):
                c = c - 1.5
            else:
                c = c + v * math.log(v)
        return c

    def get_best_one_look_ahead_guess(self, secrets):
        max_total_fan_out = -1
        final_guess = None
        for g in secrets:
            fm = self.get_fan_out_map_detailed(g, secrets)
            min_diff_guess = 0
            total_max_fan_out = 0
            for k, v in fm.items():
                if len(v) == 1:
                    total_max_fan_out += 1
                elif len(v) == 2:
                    total_max_fan_out += 2
                else:
                    # Max fan-out for a particular subset
                    max_fan_out = -1
                    best_subfm = None
                    best_subguess = None
                    for g1 in secrets:
                        fm1 = self.get_fan_out_map(g1, v)
                        if len(fm1) > max_fan_out:
                            max_fan_out = len(fm1)
                            best_subfm = fm1
                            best_subguess = g1
                    total_max_fan_out += max_fan_out
            if total_max_fan_out > max_total_fan_out:
                max_total_fan_out = total_max_fan_out
                final_guess = g

        return final_guess

    def get_next_guess(self, secrets, feedbacks):
        if len(secrets) == 1 or len(secrets) == 2:
            return secrets[0]

        guesses = self.all_secrets

        # When this strategy is used from third guess onwards
        # {1: 1, 2: 11, 3: 68, 4: 590, 5: 2394, 6: 1873, 7: 102, 8: 1}
        # Total Secrets: 5040, Total Turns: 26517, Average Game Length: 5.2613

        # When this strategy is used from second guess onwards
        # {1: 1, 2: 7, 3: 70, 4: 647, 5: 2397, 6: 1816, 7: 101, 8: 1}
        # Total Secrets: 5040, Total Turns: 26409, Average Game Length: 5.2399

        # if len(feedbacks) == 3 and len(secrets) > 75 and len(secrets) < 100:
        #     ret_guess = self.get_best_one_look_ahead_guess(secrets)
        # {1: 1, 2: 7, 3: 70, 4: 647, 5: 2399, 6: 1816, 7: 100}
        # Total Secrets: 5040, Total Turns: 26404, Average Game Length: 5.2389

        # if len(feedbacks) == 3 and len(secrets) >= 50 and len(secrets) <= 100:
        #     ret_guess = self.get_best_one_look_ahead_guess(secrets)
        # {1: 1, 2: 7, 3: 70, 4: 647, 5: 2405, 6: 1809, 7: 101}
        # Total Secrets: 5040, Total Turns: 26399, Average Game Length: 5.2379

        # BEST OPTION
        # if len(feedbacks) == 3 and len(secrets) >= 50 and len(secrets) <= 100:
        #     ret_guess = self.get_best_one_look_ahead_guess(secrets)
        # else:
        #     ret_guess = self.get_min_cost_guess(secrets)
        # c = c - 1.5

        # {1: 1, 2: 7, 3: 74, 4: 712, 5: 2309, 6: 1796, 7: 141}
        # Total Secrets: 5040, Total Turns: 26393, Average Game Length: 5.2367
        # BEST OPTION

        ret_guess = None

        if len(feedbacks) == 3 and len(secrets) >= 50 and len(secrets) <= 100:
            ret_guess = self.get_best_one_look_ahead_guess(secrets)
        else:
            ret_guess = self.get_min_cost_guess(secrets)

        # A list can't be a key in the hashmap, so we are converting it to a string.
        # When playing the game, you need to use same approach before you look into hashmap.
        # If at all, you need to get the actual list from the hashmap key, you can do eval(k)
        self.guess_map[str(feedbacks)] = ret_guess

        return ret_guess

    def save_cache(self):
        with open("cache_guesses.json", "w") as f:
            f.write(json.dumps(self.guess_map))

    def solve(self):
        turns_map = {}

        t1 = time.time()
        self.__solve("0123", self.all_secrets, 1, turns_map, [])
        t2 = time.time()

        print(f"Total run time: {t2-t1:>0.5} seconds.")
        print(turns_map)
        print(f"Length of guess_map: {len(self.guess_map)}")

    def __solve(self, guess, secrets, level, t_m, feedbacks):
        if len(secrets) == 0:
            return

        m = {}
        for s in secrets:
            x = self.get_bulls_n_cows(s, guess)
            if x not in m:
                m[x] = 1
            else:
                m[x] += 1
        if self.answer in m:
            if level not in t_m:
                t_m[level] = 1
            else:
                t_m[level] += 1
            del m[self.answer]

        F = [
            (0, 0),
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (1, 0),
            (1, 1),
            (1, 2),
            (1, 3),
            (2, 0),
            (2, 1),
            (2, 2),
            (3, 0),
            (4, 0),
        ]
        for f in F:
            if f in m:
                remaining = [
                    s
                    for s in secrets
                    if self.is_solution_possible(s, guess, f[0], f[1])
                ]
                n_feedbacks = feedbacks.copy()
                n_feedbacks.append(f)
                next_guess = self.get_next_guess(remaining, n_feedbacks)
                self.__solve(next_guess, remaining, level + 1, t_m, n_feedbacks)


if __name__ == "__main__":
    bc = BullsNCows()
    bc.solve()
    bc.save_cache()
