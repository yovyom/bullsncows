from itertools import permutations, combinations


def get_secrets_to_try(used, unused):
    p = 4
    d = 10
    placeholder_char = "x"

    def get_placeholder_list(p):
        return [placeholder_char for i in range(p)]

    def get_placeholder_string(p):
        return "".join(get_placeholder_list(p))

    def fill_remaining(s, unused):
        ret = list(s)
        j = 0
        for i in range(len(ret)):
            if ret[i] == placeholder_char:
                ret[i] = unused[j]
                j = j + 1
        return "".join(ret)

    u = len(used)
    high = p
    low = max([0, p - (d - u)])
    to_try = []
    for i in range(low, high + 1):
        if i == 0:
            to_try.append(fill_remaining(get_placeholder_string(p), unused))
        else:
            # P gives you one or more (upto p) digits from 'used'
            # symbols and C give you indices where those digits
            # have to be put. If symbols given by permutation are less than
            # p, rest are filled with any symbols from 'unused' set.
            P = list(permutations(used, i))
            C = list(combinations(list(range(p)), i))
            for i in range(len(P)):
                for j in range(len(C)):
                    l = get_placeholder_list(p)
                    for k in range(len(C[j])):
                        l[C[j][k]] = P[i][k]
                    to_try.append(fill_remaining("".join(l), unused))

    return to_try


if __name__ == "__main__":
    used = "0123"
    unused = "456789"
    for u in range(7):
        if u != 0:
            used = used + unused[0]
            unused = unused[1:]
        x = get_secrets_to_try(used, unused)
        print(f"Secrets to try with {4+u:>2} used symbols: {len(x):>4}")
